﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace OneSystemParsing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // test html decode
            //var videoPaths = new List<string>{ @"D:\advance\engish - копия\begginer\hide\01-On _ In _ At.mp4",
            //                                            @"D:\advance\engish - копия\begginer\hide\02-Объектные местоимения.mp4",
            //                                            @"D:\advance\engish - копия\begginer\hide\03-Like+v-ing.mp4"};
            //var result = SystemParsing.ParsePage(@"D:\advance\engish - копия\begginer\04 - копия\copy.html",
            //                                     @"D:\advance\engish\begginer\deleteselectors.txt",
            //                                     videoPaths);

            //File.WriteAllText(@"D:\advance\engish - копия\begginer\04 - копия\copy_parsed.html", result);
        }

        private void mainPageButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == true)
            {
                mainPageTextBox.Text = ofd.FileName;
            }
        }

        private void projectFolderButton_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new System.Windows.Forms.FolderBrowserDialog();
            var result = fbd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                projectFolderTextBox.Text = fbd.SelectedPath;
            }
        }

        private void addLessonButton_Click(object sender, RoutedEventArgs e)
        {
            var lessonWindow = new LessonWindow();
            lessonWindow.Owner = this;
            if (lessonWindow.ShowDialog() == true)
            { }
        }

        private void modifyLessonButton_Click(object sender, RoutedEventArgs e)
        {
            var lessonWindow = new LessonWindow();
            lessonWindow.Owner = this;
            if (lessonWindow.ShowDialog() == true)
            { }
        }
    }
}
