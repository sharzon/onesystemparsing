﻿using System;
using System.Collections.Generic;
using AngleSharp.Parser.Html;
using System.Net;
using System.IO;
using AngleSharp.Dom.Html;
using AngleSharp.Dom;
using System.Linq;

namespace OneSystemParsing
{
    class SystemParsing
    {
        // Todo: make all path operation via Path class

        public static string ParsePage(string pagePath, string deleteSelectorsPath, List<string> videoPaths)
        {
            var page = File.ReadAllText(pagePath);
            var currentDir = pagePath.Substring(0, pagePath.LastIndexOf('\\'));

            var result = WebUtility.UrlDecode(page);

            var parser = new HtmlParser();
            var doc = parser.Parse(result);

            DownloadAndSubstitutePdf(currentDir, doc);
            DownloadAndSubstituteAudio(currentDir, doc);
            if (videoPaths != null)
                SubstituteVideo(currentDir, doc, videoPaths);

            DeleteWasteParts(deleteSelectorsPath, doc);

            return "<!DOCTYPE html>\n" + doc.DocumentElement.OuterHtml;
        }

        private static void SubstituteVideo(string currentDir, IHtmlDocument doc, List<string> videoPaths)
        {
            var selector = @"iframe[allowfullscreen]";
            var elements = doc.QuerySelectorAll(selector);

            var videoDir = currentDir + "\\video\\";
            Directory.CreateDirectory(videoDir);

            var parser = new HtmlParser();

            for (int i = 0; i < elements.Length && i < videoPaths.Count; i++)
            {
                var videoName = videoPaths[i].Substring(videoPaths[i].LastIndexOf('\\') + 1);

                if (videoPaths[i] != (videoDir + videoName))
                    File.Copy(videoPaths[i], videoDir + videoName, true);

                IElement element = elements[i];
                var videoText = @"
                    <video height=""405"" width=""720"" style=""display: block; margin: 0 auto;"" preload controls>
                        <source src=""video\" + videoName + @""" type=""video/mp4"">
                    </video>";

                var videoElement = parser
                    .ParseFragment(videoText, element.ParentElement)
                    .OfType<IHtmlElement>()
                    .First();

                element.Parent.InsertBefore(videoElement, element);
                element.Remove();
            }
        }

        private static void DeleteWasteParts(string deleteSelectorsPath, IHtmlDocument doc)
        {
            var deleteSelectors = GetSelectorsList(deleteSelectorsPath);

            foreach (string selector in deleteSelectors)
            {
                try
                {
                    var elements = doc.QuerySelectorAll(selector);
                    foreach (var element in elements)
                        element.Remove();

                }
                catch (AngleSharp.Dom.DomException e)
                {

                    // Todo: make log writing
                }
            }
        }

        private static void DownloadAndSubstitutePdf(string currentDir, IHtmlDocument doc)
        {
            var pdfSelector = @"a[href$="".pdf""]";
            var pdfLinkList = doc.QuerySelectorAll(pdfSelector);

            Directory.CreateDirectory(currentDir + "\\pdf");

            foreach (var linkTag in pdfLinkList)
            {
                var link = linkTag.GetAttribute("href");
                var pdfName = link.Substring(link.LastIndexOf('/') + 1);
                var pdfPath = currentDir + @"\pdf\" + pdfName;

                var webClient = new WebClient();
                webClient.DownloadFile(link, pdfPath);

                linkTag.SetAttribute("href", "pdf/" + pdfName);
            }
        }

        private static void DownloadAndSubstituteAudio(string currentDir, IHtmlDocument doc)
        {
            var audioLinkSelector = "audio a";
            var audioLinkTagList = doc.QuerySelectorAll(audioLinkSelector);

            Directory.CreateDirectory(currentDir + "\\audio");

            foreach (AngleSharp.Dom.IElement linkTag in audioLinkTagList)
            {
                var link = linkTag.GetAttribute("href");
                var audioName = link.Substring(link.LastIndexOf('/') + 1);
                var audioPath = currentDir + @"\audio\" + audioName;

                var webClient = new WebClient();
                webClient.DownloadFile(link, audioPath);

                var upperDiv = linkTag.ParentElement.ParentElement.ParentElement.ParentElement;
                
                upperDiv.OuterHtml = @"
                    <audio src=""audio/" + audioName + @""" style=""width: 100%;"" controls preload>
                        <source type=""audio/mpeg"" src=""audio/" + audioName + @"""><a href=""audio/" + audioName + @""">" + audioName + @"</a>
                    </audio>";
            }
        }

        static List<string> GetSelectorsList(string filename)
        {
            var selectorsList = new List<string>();

            using (var reader = new StreamReader(filename))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    selectorsList.Add(line);
            }

            return selectorsList;
        }

    }
}
